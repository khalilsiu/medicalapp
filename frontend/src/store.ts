import {createStore, combineReducers, applyMiddleware} from 'redux'
import thunk, {ThunkDispatch} from 'redux-thunk'
import {IResultAction} from './result/actions'
import {IResultState} from './result/state'
import {resultReducer} from './result/reducer';

export interface IRootState{
    result: IResultState
}

type IRootAction = IResultAction

const rootReducer = combineReducers<IRootState>({
    result: resultReducer
})

export type Dispatch = ThunkDispatch<IRootState, null, IRootAction>

export default createStore<IRootState, IRootAction, {},{}>(
    rootReducer,
    applyMiddleware(thunk)
)