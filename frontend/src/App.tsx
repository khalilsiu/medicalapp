import React from 'react';
import styles from './App.module.css'
import Navbar from './components/Navbar';
import Main from './components/Main/Main';
import { Provider } from 'react-redux';
import store from './store';



function App() {

  return (
    <Provider store={store}>
      <div className={styles.app}>
        <Navbar />
        <Main/>
      </div>

    </Provider>
  );
}

export default App;
 