import React from 'react';
import styles from './Navbar.module.css'
import {BsCardImage} from 'react-icons/bs'
import {AiOutlineHistory,AiOutlineMenu} from 'react-icons/ai'
import {useDispatch, useSelector} from 'react-redux'
import { routeSelected } from '../result/actions';
import { IRootState } from '../store';


function Navbar() {

  const dispatch = useDispatch();
  const route = useSelector<IRootState, "OCR" | "history">(state=> state.result.route)

  const handleIconSelect = (icon: "OCR" | "history")=>{
    dispatch(routeSelected(icon))
  }

  const selectedStyle = (icon: string)=>{
    if (route === icon) return styles.iconSelected
  }


  return (
      <div className={styles.navbar}>
        <AiOutlineMenu className={styles.navIcons}/> 
        <BsCardImage className={`${styles.navIcons} ${selectedStyle('OCR')}`} onClick={()=>handleIconSelect('OCR')}/> 
        <AiOutlineHistory className={`${styles.navIcons} ${selectedStyle('history')}`} onClick={()=>handleIconSelect('history')}/> 

      </div>
  );
}

export default Navbar;
 