import React from 'react';
import styles from './ImagePane.module.css';
import { IoIosCloudUpload } from 'react-icons/io';
import {useDispatch} from 'react-redux'
import { getResult } from '../../../result/thunk';

function ImagePane() {
    const fileInput = React.createRef<HTMLInputElement>();
    const [file, setFile] = React.useState<File | null>(null);
    const [fileAtDrag, setFileAtDrag] = React.useState(false);
    const [fileInfo, setFileInfo] = React.useState({
        name: "Please upload a file.",
        size: 0,
        type: '',
    });

    const dispatch = useDispatch();

    const handleChange = () => {
        console.log("CHANGEDDDD")
        if (fileInput.current && fileInput.current.files && fileInput.current.files.length > 0) {
            const { name, size, type } = fileInput.current.files[0];
            setFile(fileInput.current.files[0]);
            setFileInfo((prevState) => ({
                ...prevState,
                name,
                size,
                type,
            }));
            dispatch(getResult(fileInput.current.files[0]))

        }
    };

    const handleDrag = (event: 'enter' | 'leave') => {
        if (event === 'enter'){
            setFileAtDrag(true);
        }else{
            setFileAtDrag(false)
        }
    }

   

    const imageSectionCSS = () => {
        if (file) return styles.imageSection;
        else if (!file && fileAtDrag) return `${styles.uploadSection} ${styles.sectionAtDrag}`
        else return styles.uploadSection;
    };

    const imageSectionStyle = () => {
        if (file) return { backgroundImage: `url(${URL.createObjectURL(file)})` };
        else return {};
    };

    return (
        <div className={styles.imagePane}>
            <div className={imageSectionCSS()} style={imageSectionStyle()}>
                <input
                    accept="image/png, image/jpeg, image/jpg"
                    className={styles.fileInput}
                    type="file"
                    ref={fileInput}
                    onChange={handleChange}
                    onDragEnter={()=>handleDrag('enter')}
                    onDragLeave={()=>handleDrag('leave')}
                />
                {!file && <div className={styles.uploadText}>
                    <IoIosCloudUpload className={styles.uploadIcon} />
                    <h4 className={styles.uploadTitle}>Drag and drop an image here</h4>
                    <h5>Or</h5>
                    <h4 className={styles.uploadTitle}>Click to browse</h4>
                </div>}
            </div>
            <div className={styles.tools}>
                <h2>INFO</h2>
                <p>File name: {fileInfo.name}</p>
                <p>Size: {fileInfo.size}</p>
                <p>Type: {fileInfo.type}</p>
            </div>
        </div>
    );
}

export default ImagePane;
