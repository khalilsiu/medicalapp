import React from 'react';
import styles from './OCR.module.css';
import { BsCardImage } from 'react-icons/bs';
import ImagePane from './ImagePane';
import {useSelector} from 'react-redux'
import { IRootState } from '../../../store';
import ResultPane from '../ResultPane/ResultPane';

function OCR() {
    const [resultText, setResultText] = React.useState('')

    const result = useSelector<IRootState, string>(state=>state.result.result)
    
    
    React.useEffect(()=>{
      setResultText(result)
    }, [result])

    return (
        <div style={{height: "93%"}}>
            <div className={styles.header}>
                <BsCardImage className={styles.headerIcon} />
                <h3>PHOTO SCAN</h3>
            </div>
            <div className={styles.panes}>
                <ImagePane />
                <ResultPane result={resultText} url="" route="OCR"/>
            </div>
        </div>
    );
}

export default OCR;
