import React from 'react';
import styles from './ResultPane.module.css';

interface IProps{
    result: string
    url: string
    route: "OCR" | "history"
}

function ResultPane(props: IProps) {
    const {result, route, url} = props;

    return (
        <div className={styles.resultPane}>
            <h3>RESULT</h3>
            <div className={styles.result}>
                {route === "history" && 
                <div className={styles.resultImage} style={{ backgroundImage: `url(${url})` }} >
                </div>}
                <div className={styles.resultText}>
                    <p >{result}</p>

                </div>
            </div>

        </div>
    );
}

export default ResultPane;
