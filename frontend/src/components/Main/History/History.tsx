import React from 'react';
import styles from './History.module.css';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../../../store';
import { AiOutlineHistory } from 'react-icons/ai';
import ResultPane from '../ResultPane/ResultPane';
import { getResults } from '../../../result/thunk';
import { Result } from '../../../result/state';

function History() {
    const results = useSelector<IRootState, Result[]>((state) => state.result.results);
    const [result, setResult] = React.useState({
        text: '',
        url: '',
    });

    const dispatch = useDispatch();

    React.useEffect(() => {
        dispatch(getResults());
    }, [dispatch]);

    const handleCardClick = (result: string, url: string) => {
        setResult({
            text: result,
            url: url,
        });
    };

    return (
        <div style={{ height: '93%' }}>
            <div className={styles.header}>
                <AiOutlineHistory className={styles.headerIcon} />
                <h3>HISTORY</h3>
            </div>
            <div className={styles.panes}>
                <div className={styles.historyPane}>
                    {results.map((result, index) => (
                        <div key={index} className={styles.historyCard} onClick={() => handleCardClick(result.text, result.url)}>
                            <h3>{index + 1}</h3>
                            <p>id: {result._id}</p>
                            <p>url: {result.url}</p>
                        </div>
                    ))}
                </div>
                <ResultPane result={result.text} url={result.url} route="history" />
            </div>
        </div>
    );
}

export default History;
