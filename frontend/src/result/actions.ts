import { Result } from "./state"

export function getResultSuccess(result: string){
    return {
        type: "GET_RESULT_SUCCESS" as "GET_RESULT_SUCCESS",
        result
    }
}

export function processing(){
    return {
        type: "PROCESSING" as "PROCESSING"
    }
}

export function getResultError(error: string){
    return {
        type: "GET_RESULT_FAIL" as "GET_RESULT_FAIL",
        error
    }
}

export function routeSelected(route: "OCR" | "history"){
    return {
        type: "ROUTE_SELECTED" as "ROUTE_SELECTED",
        route
    }
}

export function getResultsSuccess(results:Result[]){
    return {
        type: "GET_RESULTS_SUCCESS" as "GET_RESULTS_SUCCESS",
        results
    }
}




type ResultActionCreator = typeof getResultSuccess | typeof processing | typeof getResultError| typeof routeSelected | typeof getResultsSuccess

export type IResultAction = ReturnType<ResultActionCreator>