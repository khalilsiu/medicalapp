import { Dispatch } from 'redux'
import {IResultAction, getResultSuccess, processing, getResultError, getResultsSuccess} from './actions'


export function getResult(file: File){
    return async(dispatch: Dispatch<IResultAction>) => {
        dispatch(processing())
        let formData = new FormData();
        formData.append('photo', file);
        const res = await fetch(`http://localhost:8888/image`, {
            method: 'POST',
            body: formData
        })
        const response = await res.json();

        if (res.status === 200){
            dispatch(getResultSuccess(response.result));
        }else{
            dispatch(getResultError(response.error))
        }
    }
}

export function getResults(){
    return async(dispatch: Dispatch<IResultAction>) => {
        const res = await fetch(`http://localhost:8888/results`)
        const response = await res.json();

        if (res.status === 200){
            dispatch(getResultsSuccess(response.results));
        }else{
            dispatch(getResultError(response.error))
        }
    }
}