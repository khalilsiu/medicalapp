export interface IResultState{
    result: string,
    route: "OCR" | "history",
    results: Result[]
}

export interface Result{
    _id: string, url: string, text: string
}