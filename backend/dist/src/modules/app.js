"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var cors_1 = __importDefault(require("cors"));
var routes_1 = __importDefault(require("./clinicManagement/infra/http/routes"));
var origin = {
    origin: '*',
};
var app = express_1.default();
app.use(body_parser_1.default.json());
app.use(body_parser_1.default.urlencoded({ extended: true }));
app.use(cors_1.default(origin));
app.use('/v1', routes_1.default);
var port = process.env.PORT || 8080;
app.listen(port, function () {
    console.log("[App]: Listening on port " + port);
});
//# sourceMappingURL=app.js.map