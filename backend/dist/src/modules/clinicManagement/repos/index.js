"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.recordRepo = exports.userRepo = void 0;
var Records_1 = __importDefault(require("../infra/http/routes/databases/sql/models/Records"));
var User_1 = __importDefault(require("../infra/http/routes/databases/sql/models/User"));
var sqlRecordRepo_1 = __importDefault(require("./sql/sqlRecordRepo"));
var sqlUserRepo_1 = __importDefault(require("./sql/sqlUserRepo"));
exports.userRepo = new sqlUserRepo_1.default(User_1.default);
exports.recordRepo = new sqlRecordRepo_1.default(Records_1.default);
//# sourceMappingURL=index.js.map