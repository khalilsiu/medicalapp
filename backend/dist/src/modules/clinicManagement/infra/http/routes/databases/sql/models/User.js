"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var sequelize_1 = require("sequelize");
var sqlConnection_1 = __importDefault(require("../sqlConnection"));
// eslint-disable-next-line import/no-cycle
var Records_1 = __importDefault(require("./Records"));
var UserModel = sqlConnection_1.default.define('users', {
    id: {
        type: sequelize_1.DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: sequelize_1.Sequelize.literal('uuid_generate_v4()'),
    },
    email: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    passwordHash: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    clinicName: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    phone: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    address: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
});
UserModel.hasMany(Records_1.default, { as: 'record', foreignKey: 'users_id' });
exports.default = UserModel;
//# sourceMappingURL=User.js.map