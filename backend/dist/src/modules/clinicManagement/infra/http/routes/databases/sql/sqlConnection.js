"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sequelize_1 = require("sequelize");
var instance;
function getInstance() {
    if (instance) {
        return instance;
    }
    instance = new sequelize_1.Sequelize('postgres://anthonysiu:dbs123@localhost/medicalapp', {
        host: 'localhost',
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000,
        },
        logging: false,
    });
    return instance;
}
exports.default = getInstance();
//# sourceMappingURL=sqlConnection.js.map