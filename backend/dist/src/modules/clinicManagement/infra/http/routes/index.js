"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var records_1 = require("../../../usecases/records");
var users_1 = require("../../../usecases/users");
var router = express_1.default.Router();
router.use('/user', users_1.userController.router());
router.use('/record', records_1.recordController.router());
exports.default = router;
//# sourceMappingURL=index.js.map