"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var sequelize_1 = require("sequelize");
var sqlConnection_1 = __importDefault(require("../sqlConnection"));
var RecordModel = sqlConnection_1.default.define('records', {
    id: {
        type: sequelize_1.DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: sequelize_1.Sequelize.literal('uuid_generate_v4()'),
    },
    userId: {
        type: sequelize_1.DataTypes.STRING,
        references: {
            model: 'users',
            key: 'id',
        },
        allowNull: false,
    },
    doctorName: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    patientName: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    diagnosis: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    medication: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    consultationFee: {
        type: sequelize_1.DataTypes.DECIMAL,
        allowNull: false,
    },
    hasFollowUpConsultation: {
        type: sequelize_1.DataTypes.BOOLEAN,
        allowNull: false,
    },
});
exports.default = RecordModel;
//# sourceMappingURL=Records.js.map