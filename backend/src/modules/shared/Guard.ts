/* eslint-disable no-restricted-syntax */
export interface IGuardResult {
    succeeded: boolean;
    message?: string;
}

export type GuardArgumentCollection = IGuardArgument[];

export interface IGuardArgument {
    argument: any;
    argumentName: string;
}

export class Guard {
    public static againstNullOrUndefined(argument: unknown, argumentName: string): IGuardResult {
        if (argument === null || argument === undefined) {
            return { succeeded: false, message: `${argumentName} is null or undefined` };
        }
        return { succeeded: true };
    }

    public static againstNullOrUndefinedBulk(args: GuardArgumentCollection): IGuardResult {
        for (const arg of args) {
            const result = this.againstNullOrUndefined(arg.argument, arg.argumentName);
            if (!result.succeeded) return result;
        }

        return { succeeded: true };
    }
}
