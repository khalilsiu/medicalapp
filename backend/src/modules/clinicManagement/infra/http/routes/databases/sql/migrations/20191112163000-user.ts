import { QueryInterface, DataTypes } from 'sequelize';

export async function up(query: QueryInterface) {
    try {
        return query.createTable('users', {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                comment: 'Id of the instance',
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true,
                comment: 'Unique email of user',
            },
            passwordHash: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            clinicName: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            phone: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: false,
                comment: 'Date of creation',
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: false,
            },
            deletedAt: DataTypes.DATE,
        });
    } catch (e) {
        return Promise.reject(e);
    }
}

/**
 * function that sequelize-cli runs if you want to remove this migration from your database
 * */
export async function down(query: QueryInterface) {
    try {
        return query.dropTable('users');
    } catch (e) {
        return Promise.reject(e);
    }
}
