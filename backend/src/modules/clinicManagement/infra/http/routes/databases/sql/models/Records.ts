import { DataTypes, Sequelize } from 'sequelize';
import sequelize from '../sqlConnection';

const RecordModel = sequelize.define('records', {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.literal('uuid_generate_v4()'),
    },
    userId: {
        type: DataTypes.STRING,
        references: {
            model: 'users',
            key: 'id',
        },
        allowNull: false,
    },
    doctorName: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    patientName: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    diagnosis: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    medication: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    consultationFee: {
        type: DataTypes.DECIMAL,
        allowNull: false,
    },
    hasFollowUpConsultation: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
    },
});

export default RecordModel;
