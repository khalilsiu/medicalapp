import { QueryInterface, DataTypes } from 'sequelize';

export async function up(query: QueryInterface) {
    try {
        return query.createTable('records', {
            id: {
                type: DataTypes.UUID,
                primaryKey: true,
                defaultValue: DataTypes.UUIDV4,
                allowNull: false,
                comment: 'Id of the instance',
            },
            userId: {
                type: DataTypes.UUID,
                references: {
                    model: 'users',
                    key: 'id',
                },
                allowNull: false,
            },
            doctorName: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            patientName: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            diagnosis: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            medication: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            consultationFee: {
                type: DataTypes.DECIMAL,
                allowNull: false,
            },
            hasFollowUpConsultation: {
                type: DataTypes.BOOLEAN,
                allowNull: false,
            },
            createdAt: {
                type: DataTypes.DATE,
                allowNull: false,
                comment: 'Date of creation',
            },
            updatedAt: {
                type: DataTypes.DATE,
                allowNull: false,
                comment: 'Date of the last update',
            },
            deletedAt: DataTypes.DATE,
        });
    } catch (e) {
        return Promise.reject(e);
    }
}

/**
 * function that sequelize-cli runs if you want to remove this migration from your database
 * */
export async function down(query: QueryInterface) {
    try {
        return query.dropTable('records');
    } catch (e) {
        return Promise.reject(e);
    }
}
