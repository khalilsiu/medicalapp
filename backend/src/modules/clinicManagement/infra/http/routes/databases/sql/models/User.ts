import { DataTypes, Sequelize } from 'sequelize';
import sequelize from '../sqlConnection';
// eslint-disable-next-line import/no-cycle
import RecordModel from './Records';

const UserModel = sequelize.define('users', {
    id: {
        type: DataTypes.UUID,
        primaryKey: true,
        allowNull: false,
        defaultValue: Sequelize.literal('uuid_generate_v4()'),
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    passwordHash: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    clinicName: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    phone: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    address: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});

UserModel.hasMany(RecordModel, { as: 'record', foreignKey: 'userId' });

export default UserModel;
