import { Sequelize } from 'sequelize';

let instance: Sequelize;
function getInstance() {
    if (instance) {
        return instance;
    }

    instance = new Sequelize('postgres://anthonysiu:dbs123@localhost/medicalapp', {
        host: 'localhost',
        pool: {
            max: 5,
            min: 0,
            acquire: 30000,
            idle: 10000,
        },
        logging: false,
    });

    return instance;
}

export default getInstance();
