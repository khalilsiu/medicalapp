import express from 'express';
import { recordController } from '../../../usecases/records';
import { userController } from '../../../usecases/users';

const router = express.Router();

router.use('/user', userController.router());
router.use('/record', recordController.router());

export default router;
