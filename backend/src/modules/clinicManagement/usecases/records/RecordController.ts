import express, { Request, Response } from 'express';
// import RecordDetails from '../../domain/records/RecordDetails.interface';
import RecordDTO from '../../dtos/RecordDTO';
import RecordUseCase from './RecordUseCase';

export default class RecordController {
    constructor(private useCase: RecordUseCase) {
        this.useCase = useCase;
    }

    public router = () => {
        const router = express.Router();
        router.post('/', this.createRecord);
        router.get('/', this.getRecordsByUserId);
        return router;
    };

    private createRecord = async (req: Request, res: Response) => {
        const {
            userId,
            doctorName,
            patientName,
            diagnosis,
            medication,
            consultationFee,
            hasFollowUpConsultation,
        } = req.body;

        if (
            !userId ||
            !doctorName ||
            !patientName ||
            !diagnosis ||
            !medication ||
            !consultationFee ||
            !hasFollowUpConsultation
        ) {
            res.status(422).json({ msg: `A request param is missing` });
        } else {
            try {
                const dto: RecordDTO = {
                    userId,
                    doctorName,
                    patientName,
                    diagnosis,
                    medication,
                    consultationFee,
                    hasFollowUpConsultation,
                };

                await this.useCase.createRecord(dto);
                res.sendStatus(201);
            } catch (e) {
                res.status(500).json({ msg: e.toString() });
            }
        }
    };

    private getRecordsByUserId = async (req: Request, res: Response) => {
        const { userId } = req.query;
        if (!userId) {
            res.status(422).json({ msg: `A request param element is missing` });
        }
        try {
            const records = await this.useCase.getRecordsByUserId(userId as string);
            res.status(200).json(records);
        } catch (e) {
            res.status(500).json({ msg: e.toString() });
        }
    };
}
