import { recordRepo } from '../../repos';
import RecordController from './RecordController';
import RecordUseCase from './RecordUseCase';

export const recordUseCase = new RecordUseCase(recordRepo);
export const recordController = new RecordController(recordUseCase);
