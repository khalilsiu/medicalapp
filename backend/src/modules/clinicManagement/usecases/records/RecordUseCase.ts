import Record from '../../domain/records/Record';
import RecordDTO from '../../dtos/RecordDTO';
import RecordRepo from '../../repos/RecordRepo.interface';

export default class RecordUseCase {
    constructor(private recordRepo: RecordRepo) {
        this.recordRepo = recordRepo;
    }

    public async createRecord(dto: RecordDTO) {
        try {
            const record = new Record(dto);
            await this.recordRepo.save(record);
            return record;
        } catch (e) {
            throw new Error(`[Record]: Unable to create record, ${e}`);
        }
    }

    public async getRecordsByUserId(userId: string) {
        try {
            return await this.recordRepo.getRecordsByUserId(userId);
        } catch (e) {
            throw new Error(`[User]: Unable to get records by user id, ${e}`);
        }
    }
}
