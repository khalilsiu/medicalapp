import User from '../../domain/users/User';
import UserDTO from '../../dtos/UserDTO';
import UserRepo from '../../repos/UserRepo.interface';

export default class UserUseCase {
    constructor(private userRepo: UserRepo) {
        this.userRepo = userRepo;
    }

    public async createUser(dto: UserDTO) {
        try {
            const userAlreadyExists = await this.userRepo.checkExist(dto.email);
            if (userAlreadyExists) {
                throw new Error('Email is already registered');
            }
            const user = new User({ ...dto, passwordHash: 'asdfasdf' });
            await this.userRepo.save(user);
            return user;
        } catch (e) {
            throw new Error(`[User]: Unable to create user, ${e}`);
        }
    }

    public async getUserById(id: string) {
        try {
            return await this.userRepo.getUserById(id);
        } catch (e) {
            throw new Error(`[User]: Unable to get user by id, ${e}`);
        }
    }
}
