import express, { Request, Response } from 'express';
import User from './UserUseCase';
import UserDTO from '../../dtos/UserDTO';

export default class UserController {
    constructor(private useCase: User) {
        this.useCase = useCase;
    }

    public router = () => {
        const router = express.Router();
        router.post('/', this.createUser);
        router.get('/', this.getUserById);
        return router;
    };

    private getUserById = async (req: Request, res: Response) => {
        const { id } = req.query;

        if (id === undefined || id.length === 0) {
            res.status(422).json({ isSuccess: false, msg: 'Please provide clinic ID' });
        } else {
            try {
                const user = await this.useCase.getUserById(id as string);
                res.status(200).json({
                    id: user.id,
                    email: user.email,
                    clinicName: user.clinicName,
                    phone: user.phone,
                    address: user.address,
                });
            } catch (e) {
                res.status(500).json({ msg: e.toString() });
            }
        }
    };

    private createUser = async (req: Request, res: Response) => {
        const { email, password, clinicName, phone, address } = req.body;

        if (!email || !password || !clinicName || !phone || !address) {
            res.status(422).json({ msg: 'A request body element is missing' });
        } else {
            try {
                const dto: UserDTO = {
                    email,
                    password,
                    clinicName,
                    phone,
                    address,
                };

                await this.useCase.createUser(dto);
                res.sendStatus(201);
            } catch (e) {
                res.status(500).json({ msg: e.toString() });
            }
        }
    };
}
