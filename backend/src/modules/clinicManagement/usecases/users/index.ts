import { userRepo } from '../../repos';
import UserUseCase from './UserUseCase';
import UserController from './UserController';

export const userUseCase = new UserUseCase(userRepo);
export const userController = new UserController(userUseCase);
