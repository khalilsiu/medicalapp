import RecordModel from '../infra/http/routes/databases/sql/models/Records';
import UserModel from '../infra/http/routes/databases/sql/models/User';
import SqlRecordRepo from './sql/sqlRecordRepo';
import SqlUserRepo from './sql/sqlUserRepo';

export const userRepo = new SqlUserRepo(UserModel);
export const recordRepo = new SqlRecordRepo(RecordModel);
