import Record from '../domain/records/Record';
import RecordDetails from '../domain/records/RecordDetails.interface';

export default interface RecordRepo {
    getRecordsByUserId: (clinicId: string) => Promise<RecordDetails[]>;
    // getRecordByRecordId: (id: string) => Promise<RecordDetails>;
    save: (record: Record) => Promise<void>;
}
