import UserModel from '../../infra/http/routes/databases/sql/models/User';
import UserRepo from '../UserRepo.interface';
import User from '../../domain/users/User';
import UserDetails from '../../domain/users/UserDetails.interface';
import RecordModel from '../../infra/http/routes/databases/sql/models/Records';

type UserModelType = typeof UserModel;

export default class SqlUserRepo implements UserRepo {
    constructor(private userModel: UserModelType) {
        this.userModel = userModel;
        this.userModel.hasMany(RecordModel, { foreignKey: 'userId' });
    }

    getUserById = async (id: string) => {
        const user = await this.userModel.findOne({
            where: {
                id,
            },
        });
        if (user) {
            return user.toJSON() as UserDetails;
        }
        throw new Error(`${id} does not correspond to any user`);
    };

    checkExist = async (email: string) => {
        const user = await this.userModel.findOne({
            where: {
                email,
            },
        });
        return !!user === true;
    };

    save = async (user: User) => {
        const userDataObject = {
            email: user.email,
            passwordHash: user.passwordHash,
            clinicName: user.clinicName,
            phone: user.phone,
            address: user.address,
        };
        await this.userModel.create(userDataObject);
    };
}
