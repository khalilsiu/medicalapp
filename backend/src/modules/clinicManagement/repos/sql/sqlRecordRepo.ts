import RecordModel from '../../infra/http/routes/databases/sql/models/Records';
import RecordRepo from '../RecordRepo.interface';
import Record from '../../domain/records/Record';
import UserModel from '../../infra/http/routes/databases/sql/models/User';

type RecordModelType = typeof RecordModel;

export default class SqlRecordRepo implements RecordRepo {
    constructor(private recordModel: RecordModelType) {
        this.recordModel = recordModel;
        this.recordModel.belongsTo(UserModel, { foreignKey: 'userId' });
    }

    // getRecordByRecordId = async (recordId: string) => {
    //     const record = await this.recordModel.findOne({
    //         where: {
    //             id: recordId,
    //         },
    //     });
    //     if (record) {
    //         return record.toJSON();
    //     }
    //     throw new Error(`${recordId} does not correspond to any user`);
    // };

    getRecordsByUserId = async (userId: string) => {
        UserModel.hasMany(RecordModel, { foreignKey: 'userId' });
        await this.recordModel.sync({ alter: true });
        const rawRecords = await this.recordModel.findAll({
            where: {
                userId,
            },
            include: [UserModel],
        });

        const records = rawRecords.map((record) => {
            const {
                id,
                doctorName,
                patientName,
                diagnosis,
                medication,
                consultationFee,
                hasFollowUpConsultation,
                user,
            } = record.toJSON() as any;
            const { clinicName, phone, address } = user;
            return {
                id,
                userId,
                clinicName,
                phone,
                address,
                doctorName,
                patientName,
                diagnosis,
                medication,
                consultationFee,
                hasFollowUpConsultation,
            };
        });

        return records;
    };

    save = async (record: Record) => {
        const recordDataObject = {
            userId: record.userId,
            doctorName: record.doctorName,
            patientName: record.patientName,
            diagnosis: record.diagnosis,
            medication: record.medication,
            consultationFee: record.consultationFee,
            hasFollowUpConsultation: record.hasFollowUpConsultation,
        };
        await this.recordModel.create(recordDataObject);
    };
}
