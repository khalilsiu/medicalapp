import User from '../domain/users/User';
import UserDetails from '../domain/users/UserDetails.interface';

export default interface UserRepo {
    getUserById: (id: string) => Promise<UserDetails>;
    checkExist: (email: string) => Promise<boolean>;
    save: (user: User) => Promise<void>;
}
