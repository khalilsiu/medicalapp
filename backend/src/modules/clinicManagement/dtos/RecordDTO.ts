export default interface RecordDTO {
    userId: string;
    doctorName: string;
    patientName: string;
    diagnosis: string;
    medication: string;
    consultationFee: string;
    hasFollowUpConsultation: string;
}
