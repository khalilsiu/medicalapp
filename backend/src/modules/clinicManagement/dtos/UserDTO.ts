export default interface UserDTO {
    email: string;
    password: string;
    clinicName: string;
    phone: string;
    address: string;
}
