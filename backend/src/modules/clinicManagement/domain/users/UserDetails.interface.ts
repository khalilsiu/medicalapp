export default interface UserDetails {
    id: string;
    email: string;
    clinicName: string;
    phone: string;
    address: string;
}
