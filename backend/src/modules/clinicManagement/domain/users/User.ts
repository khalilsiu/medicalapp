import { Guard } from '../../../shared/Guard';

interface UserProps {
    email: string;
    passwordHash: string;
    clinicName: string;
    phone: string;
    address: string;
}

export default class User {
    get email() {
        return this.props.email;
    }

    get passwordHash() {
        return this.props.passwordHash;
    }

    get phone() {
        return this.props.phone;
    }

    get address() {
        return this.props.address;
    }

    get clinicName() {
        return this.props.clinicName;
    }

    constructor(private props: UserProps) {
        this.props = props;
    }

    public create = (props: UserProps) => {
        const guardResult = Guard.againstNullOrUndefinedBulk([
            { argument: props.email, argumentName: 'email' },
            { argument: props.passwordHash, argumentName: 'passwordHash' },
            { argument: props.clinicName, argumentName: 'clinicName' },
            { argument: props.phone, argumentName: 'phone' },
            { argument: props.address, argumentName: 'address' },
        ]);

        if (!guardResult.succeeded) {
            return { isSuccess: false, msg: guardResult.message };
        }

        return new User(this.props);
    };
}
