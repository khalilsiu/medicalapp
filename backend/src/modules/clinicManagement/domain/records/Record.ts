import { Guard } from '../../../shared/Guard';

interface RecordProps {
    userId: string;
    doctorName: string;
    patientName: string;
    diagnosis: string;
    medication: string;
    consultationFee: string;
    hasFollowUpConsultation: string;
}

export default class Record {
    get userId() {
        return this.props.userId;
    }

    get doctorName() {
        return this.props.doctorName;
    }

    get patientName() {
        return this.props.patientName;
    }

    get diagnosis() {
        return this.props.diagnosis;
    }

    get medication() {
        return this.props.medication;
    }

    get consultationFee() {
        return this.props.consultationFee;
    }

    get hasFollowUpConsultation() {
        return this.props.hasFollowUpConsultation;
    }

    constructor(private props: RecordProps) {
        this.props = props;
    }

    public create = (props: RecordProps) => {
        const guardResult = Guard.againstNullOrUndefinedBulk([
            { argument: props.userId, argumentName: 'userId' },
            { argument: props.doctorName, argumentName: 'doctorName' },
            { argument: props.medication, argumentName: 'medication' },
            { argument: props.patientName, argumentName: 'patientName' },
            { argument: props.diagnosis, argumentName: 'diagnosis' },
            { argument: props.medication, argumentName: 'medication' },
            { argument: props.consultationFee, argumentName: 'consultationFee' },
            { argument: props.hasFollowUpConsultation, argumentName: 'hasFollowUpConsultation' },
        ]);

        if (!guardResult.succeeded) {
            return { isSuccess: false, msg: guardResult.message };
        }

        return new Record(this.props);
    };
}
