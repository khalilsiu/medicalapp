export default interface RecordDetails {
    id: string;
    userId: string;
    clinicName: string;
    phone: string;
    address: string;
    doctorName: string;
    patientName: string;
    diagnosis: string;
    medication: string;
    consultationFee: string;
    hasFollowUpConsultation: string;
}
