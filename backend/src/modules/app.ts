import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import router from './clinicManagement/infra/http/routes';

const origin = {
    origin: '*',
};

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors(origin));

app.use('/v1', router);

const port = process.env.PORT || 8080;

app.listen(port, () => {
    console.log(`[App]: Listening on port ${port}`);
});
