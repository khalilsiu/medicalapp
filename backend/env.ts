import dotenv from 'dotenv'
import * as path from 'path';

dotenv.config({ path: path.join(__dirname, '.env') });

module.exports = {
    LOG_LEVEL: process.env.LOG_LEVEL,
    MONGO_URI: process.env.MONGO_URI,
    API_KEY: process.env.API_KEY
}
