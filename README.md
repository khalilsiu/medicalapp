This project is built for the coding test for TCV

## What this app does?

This is a simple photo scanning app to detect texts in images using OCR. Users are able to upload any image (png, jpeg, jpg formats) and get the text results right away. Users can also view the scan history at the history tab.

## `cd frontend`

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## `cd backend`

### `npm start dev`

Launches the node.js server at port 8888 to listen to routes

## /image

| **Description** |  |
|-----------------|-----------------------------------|
| **Method** | POST |

#### Example Request
	POST {base}/image

	Request body:
		{
			"photo": formData(file)
		}

#### Example Response 200 
	{
		"result": "THE HARDER YOU WORK FOR\nSOMETHING, THE GREATER YOU'LL\nFEEL WHEN YOU ACHIEVE IT.\nSUCCESS.Com\n"
	}

#### Example Response 500
    {
		"error": "Image upload failed."
	}


## /results

| **Description** |  |
|-----------------|-----------------------------------|
| **Method** | GET |

#### Example Request
	GET {base}/results

#### Example Response 200 
	{
		"results": [{
            "_id": "5f344c7f99b9215c33d033f9",
            "url": "https://firebasestorage.googleapis.com/v0/b/prudentialtest.appspot.com/o/5iwi7nkdrt4sf4.jpeg?alt=media",
            "text": "THE HARDER YOU WORK FOR\nSOMETHING, THE GREATER YOU'LL\nFEEL WHEN YOU ACHIEVE IT.\nSUCCESS.Com\n",
            "__v": 0
        }]
	}

#### Example Response 500
    {
		"error": "Results fetching failed."
	}


## Architecture

![Image of Yaktocat](https://firebasestorage.googleapis.com/v0/b/prudentialtest.appspot.com/o/21fazd9ldkdsgitkr.png?alt=media)

### Data flow

1. Image data (Form data) is sent from the Front End via HTTP POST request 
2. Controller uploads the file to File Storage, receives data from the path and passes it to the Interactor 
3. Interactor receives the data and make function calls to retrieve the base64 encoded string from the file storage (Firebase)
4. Interactor processes the image with the OCR Engine via POST request
5. Interactor saves the results to database (Mongoose)
6. Result data with the image URL and the result text is being passed back to the Front End as a response

### Advantage
- Interfaces are being implemented between services to define the methods being used 
  - Single responsibility: a module is only responsible for one use case
- Information hiding: 
  - Logic of the dependent component will not be exposed and changed accidentally by the used one
- Modules are interchangeable: 
  - Easily switch between different databases, OCR engine and File storages by defining the interfaces and abstract out the implementation in their own classes
- Scalable: 
  - Easily include more functions in the interface to support (e.g. Face recognition)
- Testable: 
  - Components are easily mocked for unit tests
